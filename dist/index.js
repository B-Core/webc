var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { v4 } from 'uuid';
import errors from './errors/index';
var WebComponent = /** @class */ (function (_super) {
    __extends(WebComponent, _super);
    function WebComponent(params) {
        var _this = _super.call(this) || this;
        _this.uuid = v4();
        errors.checkError({
            fn: function () { return params.name !== null; },
            message: "WebComponent.new: WebComponent.name should be defined",
        });
        _this.name = params.name;
        return _this;
    }
    return WebComponent;
}(HTMLElement));
//# sourceMappingURL=index.js.map