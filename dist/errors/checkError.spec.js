var _this = this;
import test from "ava";
import { stub } from "sinon";
var testsParams = [
    {
        fn: function () { return true; },
        message: null,
        log: null,
        vars: {
            stub: stub(console, "error"),
        },
        expected: function (t) {
            t.true(_this.vars.stub.called);
        },
    },
];
testsParams.forEach(function (o) {
    test("checkError -> case: " + JSON.stringify(o), o.expected.bind(o));
});
//# sourceMappingURL=checkError.spec.js.map