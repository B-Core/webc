// checkError returns an error object on a predicate
// if the predicate returns an error it logs or throw the error
// and create the appropriate type of error
export default (function (params) {
    var err = null;
    var fn = params.fn, _a = params.message, message = _a === void 0 ? null : _a, _b = params.log, log = _b === void 0 ? true : _b;
    try {
        fn();
    }
    catch (e) {
        e.message = message ? message : e.message;
        err = e;
    }
    if (log && err)
        console.error(err);
    else if (err && !log)
        throw err;
    return err;
});
//# sourceMappingURL=checkError.js.map