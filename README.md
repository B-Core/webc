# WebComponent-model

A basic class to create web components

# Installation

```javascript
npm install @webc/model
```

where tou want to use it

```javascript
import WebComponent from "@webc/model"

const webc = new WebComponent(options);
```