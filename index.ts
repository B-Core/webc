import { v4 } from 'uuid';

import errors from './errors/index';

interface WebComponentParams {
  name: string
}

interface WebComponent {
  uuid: string
  name: string
}

class WebComponent extends HTMLElement implements WebComponent {
  constructor(params: WebComponentParams) {
    super();
    this.uuid = v4();

    errors.checkError({
      fn: () => params.name !== null,
      message: "WebComponent.new: WebComponent.name should be defined",
    });

    this.name = params.name;
  }
}