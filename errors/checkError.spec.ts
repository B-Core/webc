import test from "ava";
import { stub } from "sinon";

import checkError from "./checkError";

const testsParams = [
  {
    fn: () => true,
    message: null,
    log: null,
    vars: {
      stub: stub(console, "error"),
    },
    expected: (t) => {
      t.true(this.vars.stub.called);
    },
  },
  // {
  //   fn: () => false,
  //   message: null,
  //   log: null,
  //   expected: {
  //     ret: null,
  //   }
  // }
];

testsParams.forEach((o) => {
  test(`checkError -> case: ${JSON.stringify(o)}`, o.expected.bind(o));
})