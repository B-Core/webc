export type error = Error | null;

// checkError returns an error object on a predicate
// if the predicate returns an error it logs or throw the error
// and create the appropriate type of error
export default (params: { fn: any, message?: string,log?: Boolean }): error => {
  let err: error = null;
  const { fn, message = null, log = true } = params;
  try {
    fn()
  } catch(e) {
    e.message = message ? message : e.message;   
    err = e;
  }

  if (log && err) console.error(err)
  else if (err && !log) throw err;

  return err;
}